#module load anaconda/2-2.3.0
## You need regex library in your python distribution
#source activate my_root
module load enthought_python/7.3.2

forward=$1
reverse=$2
barcodes=$3
refs=$4

umi="True"
umilength=20

outdir="results/"
sep_dir="$outdir""separated_by_barcode"
comb_dir="$outdir""combined_by_pear"
align_dir="$outdir""aligned_to_ref"
merg_dir="$outdir""merged"

mkdir $outdir
mkdir "$sep_dir"  
mkdir "$comb_dir" 
mkdir "$align_dir"  
mkdir "$merg_dir" 
mkdir insertions

# File Format
# "Sample name" "forward barcode" "reverse barcode" "20bp around cutside"

############################################################

#Separate
if [ "$umi" = "False" ]
then
python separate_by_barcodes_modified.py $forward $reverse fastq $barcodes $sep_dir
else
python separate_by_barcodes_modified_UMIversion.py $forward $reverse fastq $barcodes $sep_dir $umilength
fi
echo "separation by barcodes was done"
############################################################
# Combine by Pear
while read c1 c2 c3 c4 c5; do
./pear/pear-0.9.10-bin-64/pear-0.9.10-bin-64 -f $sep_dir/"$c1"_forward_*.fastq -r $sep_dir/"$c1"_reverse_*.fastq -o $comb_dir/"$c1".fastq ; echo ""$c1" is done"
done < $barcodes

echo "combining by PEAR was done"
############################################################
# Map and get stats
while read c1 c2; do
mkdir $outdir$c1
echo $c2 | tr /a-z/ /A-Z/ >> $outdir"$c1"/$c1"_ref.txt"
grep "$c1" $barcodes > $outdir"$c1"/$c1"_barcodes.txt"
if [ "$umi" = "False" ]
then
while read f1 f2 f3 f4 f5; do
python map_togetDXMIseq.py $outdir$c1 $f1 $comb_dir $align_dir
done < $outdir"$c1"/$c1"_barcodes.txt"
else
module load mafft/7.305
while read f1 f2 f3 f4 f5; do
mkdir temp_for_umi
##Initial mapping - Not that efficient
python map_togetDXMIseq_umi.py $outdir$c1 $f1 $comb_dir temp_for_umi/
##Extract similar barcodes
python merge-umi-barcodes.py $f1 temp_for_umi/ temp_for_umi/
##Multi Align and Merge
python multi_align_by_umibarcode.py temp_for_umi/$f1".new.aligned.txt" temp_for_umi/$f1".newi.barcodes.txt" temp_for_umi/$f1".txt" temp_for_umi/$f1".seq"
python add_mapped_info.py temp_for_umi/$f1".seq" temp_for_umi/$f1".aligned.txt" $align_dir/$f1".aligned.txt"
mv temp_for_umi/ $outdir/temp_for_umi/
done < $outdir"$c1"/$c1"_barcodes.txt"
fi

##Final stats
module unload enthought_python/7.3.2
module load anaconda/2-2.3.0
## You need regex library in your python distribution
source activate my_root
python statisticsfrommapped.py $outdir"$c1"/$c1"_barcodes.txt" $align_dir $outdir"$c1"/$c1"_ref.txt" $outdir"$c1"/"$c1"
done < $refs
############################################################
mv insertions $outdir"insertions"



