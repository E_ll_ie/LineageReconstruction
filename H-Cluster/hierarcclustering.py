import sys
from collections import defaultdict
import clusterwithdist
import ioutil
from random import shuffle
import const
import visualize
class hclust:
    def __init__(self):
        self.SAMPLES = defaultdict(list)
        self.NEWSAMPLES = defaultdict(list)
        self.CENTERS = defaultdict(list)
        self.CLUSTERS = defaultdict(list)
        
        self.score_dict = defaultdict(dict)
        self.CDIST = defaultdict(dict)
        self.BELONGS = dict()
        
    ## To avoid calculating a parent go with distances
    def calculate_clusterdistances(self):
        clusternames = self.CLUSTERS.keys()    
        newscore_dict = defaultdict(dict)
        sumdist = 0 
        selfsumdist = 0.0
        for c1ind in range(len(clusternames)):    
            for c2ind in range(c1ind, len(clusternames)):
                this_cluster = self.CLUSTERS[clusternames[c1ind]]
                that_cluster = self.CLUSTERS[clusternames[c2ind]]
                tempdist = 0
                for this_sample in this_cluster:
                    for that_sample in that_cluster:
                        parent, dist = self.score_dict[this_sample][that_sample]
                        tempdist+= dist
                newscore_dict[clusternames[c1ind]][clusternames[c2ind]] = (parent , tempdist)
                newscore_dict[clusternames[c2ind]][clusternames[c1ind]] = (parent , tempdist)
                if c1ind!=c2ind:
                    sumdist += tempdist
                else:
                    selfsumdist += tempdist
        #sumdist, selfsumdist,                    
        self.score_dict = newscore_dict
        return sumdist/selfsumdist 
        
    def calculate_all_scores(self, flag):
        #self.score_dict = [[-1 for i in range(len(self.SAMPLES))] for j in range(len(self.SAMPLES))]
        sampleskeys = self.SAMPLES.keys()   
        #print sampleskeys
        for f in range(len(self.SAMPLES)):
            for g in range(f, len(self.SAMPLES)):
                if flag:
                    print self.SAMPLES[sampleskeys[f]], self.SAMPLES[sampleskeys[g]]
                parent, score = self.calc_scores_andparent(self.SAMPLES[sampleskeys[f]],self.SAMPLES[sampleskeys[g]])
                self.score_dict[sampleskeys[f]][sampleskeys[g]] = (parent, score)
                self.score_dict[sampleskeys[g]][sampleskeys[f]] = (parent, score)
                self.CDIST[sampleskeys[g]][sampleskeys[f]] = score
                self.CDIST[sampleskeys[f]][sampleskeys[g]] = score
                
    def initialize_clusters(self):
        for s in self.SAMPLES:
            self.CLUSTERS[s] = [s]
            
    
    def calculate_clusterdist(self, c1 , c2):
        counts = 0.0
        temp = 0.0
        for this_sample in self.CLUSTERS[c1]:
            for that_sample in self.CLUSTERS[c2]:
                #print this_sample, that_sample
                temp+=self.score_dict[this_sample][that_sample][1]
                counts+=1
        
        self.CDIST[c1][c2] = temp/counts
        
    
    def recalculate_clusterdist(self):
        for c1 in self.CLUSTERS:
            for c2 in self.CLUSTERS:
                if len(self.CLUSTERS[c1]) > 0 and len(self.CLUSTERS[c2])> 0:
                    self.calculate_clusterdist(c1, c2)
                


    def calc_scores_andparent(self, this_sampled, that_sampled):
        if len(this_sampled) > len(that_sampled):
            outputsum, outputparents = clusterwithdist.calc_sumdist(this_sampled, that_sampled)
            #FIXME:
            ## Not sure if normalization this way is necessary 
            #print outputsum
            #this_score = (sum(outputsum.values())*1.0/(len(this_sampled)))
            this_score = (sum(outputsum.values())*1.0/(len(that_sampled)))

            #this_score = (sum(outputsum.values())*1.0/(len(this_sampled)+len(that_sampled)))
        else:
            outputsum, outputparents = clusterwithdist.calc_sumdist(that_sampled, this_sampled)
            #print outputsum
            ## Not sure if normalization this way is necessary 
            #this_score = (sum(outputsum.values())*1.0/len(that_sampled))
            this_score = (sum(outputsum.values())*1.0/len(this_sampled))
            #this_score = (sum(outputsum.values())*1.0/(len(this_sampled)+len(that_sampled)))
            
        return outputparents, this_score
                
    def cluster(self):
        
        clusters = []
        final_clusters = []
        for f in self.CLUSTERS:
            clusters.append(f)
        while len(clusters) > 1:
            mindist = "inf"
            for i in range(len(clusters)):
                for j in range(i+1, len(clusters)):
                    c1 = clusters[i]
                    c2 = clusters[j]
                    if c1!=c2:
                        if self.CDIST[c1][c2] < mindist:
                            temptomerge =(c1, c2)
                            mindist = self.CDIST[c1][c2]
                            #print "**", c1, c2
                        elif self.CDIST[c1][c2] == mindist:
                            print c1,c2
            cl1 , cl2 = temptomerge
            #print temptomerge
            tt1 = self.CLUSTERS[cl1]   
            tt2 = self.CLUSTERS[cl2] 
            self.CLUSTERS[cl1] = self.CLUSTERS[cl1]+self.CLUSTERS[cl2]
            self.CLUSTERS[cl2] = []
            #print clusters
            clusters.remove(cl2)
            self.CDIST = defaultdict(dict)
            self.recalculate_clusterdist()
            final_clusters.append((tt1, tt2, round(mindist,2)))
        allle = []
        for e in  final_clusters:
            allle.append(e)
        #print allle
        cls = visualize.vis(allle, const.PIC)
        self.CLUSTERS = cls
        return self.calculate_clusterdistances()
 
    def cluster_b(self):
        clusters = []
        paired = []
        thresh = len(self.SAMPLES)
        final_clusters = []
        for f in self.CLUSTERS:
            clusters.append(f)
        while len(clusters) > 2:
            mindist = "inf"
            for i in range(len(clusters)):
                for j in range(i+1, len(clusters)):
                    c1 = clusters[i]
                    c2 = clusters[j]
                    if c1!=c2 and c1 not in paired and c2 not in paired:
                        if self.CDIST[c1][c2] < mindist:
                            temptomerge =(c1, c2)
                            mindist = self.CDIST[c1][c2]
            cl1 , cl2 = temptomerge
            tt1 = self.CLUSTERS[cl1]   
            tt2 = self.CLUSTERS[cl2]
            
            paired.append(cl1)
            paired.append(cl2)

                
            self.CLUSTERS[cl1] = self.CLUSTERS[cl1]+self.CLUSTERS[cl2]
            self.CLUSTERS[cl2] = []
            #print clusters
            clusters.remove(cl2)
            self.CDIST = defaultdict(dict)
            self.recalculate_clusterdist()
            final_clusters.append((tt1, tt2, round(mindist,2)))        
            if len(paired) == thresh :
                paired = []
                thresh == thresh/2
                print final_clusters
    def fill_samples(self):
        all_samples = ["530_1-1-1", "531_1-1-2", "532_1-1-3", "533_1-1-4", "534_1-2-1", "535_1-2-2", "536_1-2-3", "537_1-2-4", "538_1-3-1", "539_1-3-2", "540_1-3-3", "541_1-3-4", "542_2-1-1", "543_2-1-2", "544_2-1-3", "545_2-1-4", "546_2-2-1", "547_2-2-2", "548_2-2-3", "549_2-2-4", "550_2-3-1", "551_2-3-2", "552_2-3-3", "553_2-3-4",\
        "554_3-1-1", "555_3-1-2", "556_3-1-3", "557_3-1-4", "558_3-2-1", "559_3-2-2", "560_3-2-3", "561_3-2-4", "562_3-3-1", "563_3-3-2", "564_3-3-3", "565_3-3-4"]
        
        # "542_2-1-1", "543_2-1-2", "544_2-1-3", "545_2-1-4", "546_2-2-1", "547_2-2-2", "548_2-2-3", "549_2-2-4", "550_2-3-1", "551_2-3-2", "552_2-3-3", "553_2-3-4", "554_3-1-1", "555_3-1-2", "556_3-1-3", "557_3-1-4", "558_3-2-1", "559_3-2-2", "560_3-2-3", "561_3-2-4", "562_3-3-1", "563_3-3-2", "564_3-3-3", "565_3-3-4"]
        #const.INS_MIN =8
        #const.MAX_DIFF = 4
        #all_samples = ['531_1-1-2', '533_1-1-4', '530_1-1-1', '532_1-1-3', '535_1-2-2', '534_1-2-1', '537_1-2-4', '536_1-2-3', '539_1-3-2', '541_1-3-4', '540_1-3-3', '538_1-3-1']        
        #all_samples = ['543_2-1-2', '544_2-1-3', '545_2-1-4', '547_2-2-2', '542_2-1-1', '546_2-2-1', '552_2-3-3', '548_2-2-3', '551_2-3-2', '553_2-3-4', '550_2-3-1', '549_2-2-4']
        #all_samples = ['561_3-2-4', '565_3-3-4', '558_3-2-1', '562_3-3-1', '563_3-3-2', '564_3-3-3', '560_3-2-3', '559_3-2-2', '557_3-1-4', '554_3-1-1', '556_3-1-3', '555_3-1-2']        
        #print all_samples
        #all_samples = ["530_1-1-1", "532_1-1-3", "533_1-1-4", "534_1-2-1", "535_1-2-2", "536_1-2-3", "538_1-3-1", "539_1-3-2", "540_1-3-3", "541_1-3-4", "542_2-1-1", "544_2-1-3", "545_2-1-4", "546_2-2-1", "547_2-2-2", "548_2-2-3", "550_2-3-1", "551_2-3-2", "552_2-3-3", "553_2-3-4",\
        #"554_3-1-1", "556_3-1-3", "557_3-1-4", "558_3-2-1", "559_3-2-2", "560_3-2-3", "562_3-3-1", "563_3-3-2", "564_3-3-3", "565_3-3-4"]
        shuffle(all_samples)
        
        self.SAMPLES , ins_info_list = ioutil.extract_all_modinfo(all_samples)
        self.initialize_clusters()
        print all_samples
        self.calculate_all_scores(False)
        print "scoring is done"
        #print self.CLUSTERS
        ratio = self.cluster()
        #print "*********************************************************"
        return const.INS_MIN, const.MAX_DIFF, ratio

#H = hclust()
#H.fill_samples()


maxr = 0
for f in range(9,15):
    for k in range(0,1):
        print f,k              
        const.INS_MIN = f
        const.MAX_DIFF = k 
        lk = hclust()
        im, md, r = lk.fill_samples()
        print f,k, r
        if r > maxr:
            maxr = r
            imf = im
            mdf = md

lk = hclust()
#imf = 12
#mdf = 8
const.PIC = True
const.INS_MIN = imf
const.MAX_DIFF = mdf
#print maxr, imf, mdf
#im, md, r = 
print lk.fill_samples()
#lk.fill_samples_realkmeans()
