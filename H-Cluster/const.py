# -*- coding: utf-8 -*-

## FOR Extracting statistics
R_H = 10
MAXINSDEL = 100
CUT = 100
FIND_CUTSIDE = True

## LIMIT ON INSERTION LENGTH
INS_MIN = 3
INS_MAX = 40


## ANY LIMIT ON COUNT OR PERCENTAGE
APPLYCOUNTLIMIT = False
APPLYPERCLIMIT = False
COUNTLIMIT = [0,10000]
PERCLIMIT = [0,100]


## Just use the longest
APPLYTOPLENGTH = False

## Just use the less frequent
APPLYTOPCOUNT = False

## Top percentage for above
TOPP = 0.1


## Weight for scores when pairing up
APPLYWEIGHT = False

## The allowed difference between sequences to still get paired
MAX_DIFF = 0

PENALTY = 10.0
PIC = False
