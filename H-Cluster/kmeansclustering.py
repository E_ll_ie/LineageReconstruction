import sys
from collections import defaultdict
import clusterwithdist
import ioutil
from random import shuffle
import const
class LIKEKMEANS():
    def __init__(self):
        self.LIST_OF_SAMPLES_IDS = []
        self.SAMPLES = defaultdict(list)
        self.NEWSAMPLES = defaultdict(list)
        self.CENTERS = defaultdict(list)
        self.CLUSTERS = defaultdict(list)
        
        self.score_dict = defaultdict(dict)
        self.BELONGS = dict()
        self.KCENTERS = defaultdict(dict)
    
    def calculate_all_scores(self, flag):
        #self.score_dict = [[-1 for i in range(len(self.SAMPLES))] for j in range(len(self.SAMPLES))]
        sampleskeys = self.SAMPLES.keys()   
        #print sampleskeys
        for f in range(len(self.SAMPLES)):
            for g in range(f, len(self.SAMPLES)):
                if flag:
                    print self.SAMPLES[sampleskeys[f]], self.SAMPLES[sampleskeys[g]]
                parent, score = self.calc_scores_andparent(self.SAMPLES[sampleskeys[f]],self.SAMPLES[sampleskeys[g]])
                self.score_dict[sampleskeys[f]][sampleskeys[g]] = (parent, score)
                self.score_dict[sampleskeys[g]][sampleskeys[f]] = (parent, score)
                #print score
    ## Choose centers in each level
    ## Is this the best way?
    def choose_random_centers(self, num_clusters):
        score_dict = self.score_dict
        temp_centers = []
        temp_maxdist = 0
        for f in score_dict:
            for g in score_dict[f]:
                #print f,g, score_dict[f][g][1]
                if score_dict[f][g][1] > temp_maxdist:
                    temp_maxdist = score_dict[f][g][1]
                    temp_pair = (f,g)
        
        temp_centers.append(temp_pair[0])
        temp_centers.append(temp_pair[1])
        #print temp_centers
        while len(temp_centers) != num_clusters:
            temp_maxdist = 0
            for this_one in score_dict:
                if this_one not in temp_centers:
                    sumdist = 0
                    for c in temp_centers:
                        sumdist += score_dict[this_one][c][1]
                        if sumdist > temp_maxdist :
                            #print "hah"
                            temp_maxdist = sumdist
                            temp_thiscenter = this_one
            #print temp_thiscenter
            temp_centers.append(temp_thiscenter)
        
        for tc in temp_centers:
            self.CLUSTERS[tc] = [tc]
            self.BELONGS[tc] = tc
        return temp_centers


    ######################################################
    ## Choose centers in each level
    ## Is this the best way?
    def choose_random_centers_forrealkmeans(self, num_clusters):
        score_dict = self.score_dict
        temp_centers = []
        temp_maxdist = 0
        for f in score_dict:
            for g in score_dict[f]:
                #print f,g, score_dict[f][g][1]
                if score_dict[f][g][1] > temp_maxdist:
                    temp_maxdist = score_dict[f][g][1]
                    temp_pair = (f,g)
                    
        temp_centers.append(temp_pair[0])
        temp_centers.append(temp_pair[1])
        #print temp_centers
        while len(temp_centers) != num_clusters:
            temp_maxdist = 0
            for this_one in score_dict:
                if this_one not in temp_centers:
                    sumdist = 0
                    for c in temp_centers:
                        sumdist += score_dict[this_one][c][1]
                        if sumdist > temp_maxdist :
                            #print "hah"
                            temp_maxdist = sumdist
                            temp_thiscenter = this_one
            #print temp_thiscenter            
            temp_centers.append(temp_thiscenter)
            
        
        for i  in range(len(temp_centers)):
            self.KCENTERS[i+1] = self.SAMPLES[temp_centers[i]]
            self.CLUSTERS[i+1].append(temp_centers[i])
            self.BELONGS[temp_centers[i]] = i+1
        return temp_centers
    ######################################################



    def calc_scores_andparent(self, this_sampled, that_sampled):
        if len(this_sampled) > len(that_sampled):
            outputsum, outputparents = clusterwithdist.calc_sumdist(this_sampled, that_sampled)
            #FIXME:
            ## Not sure if normalization this way is necessary 
            #print outputsum
            this_score = (sum(outputsum.values())*1.0/len(that_sampled))
        else:
            outputsum, outputparents = clusterwithdist.calc_sumdist(that_sampled, this_sampled)
            #print outputsum
            ## Not sure if normalization this way is necessary 
            this_score = (sum(outputsum.values())*1.0/len(this_sampled))
            
        return outputparents, this_score
        
    def update_clusters_frommembers(self, clusterid):
        shuffle((self.CLUSTERS[clusterid]))
        num_members = len(self.CLUSTERS[clusterid])
        if num_members == 1:
            self.CENTERS[clusterid] = self.SAMPLES[self.CLUSTERS[clusterid][0]]
        elif num_members >= 2 :
            this_parent , dist_to_this = self.calc_scores_andparent(self.SAMPLES[self.CLUSTERS[clusterid][0]], self.SAMPLES[self.CLUSTERS[clusterid][1]])
            if num_members > 2:
                for other in range(2,num_members):
                    this_parent , dist_to_this = self.calc_scores_andparent(self.SAMPLES[self.CLUSTERS[clusterid][other]], this_parent)
        
            #self.CENTERS[clusterid] = this_parent
            return this_parent
            
            
    ######################################################
    ## Calculate Distance
    ## Real Kmeans
    def calculate_distance_fromcenters(self):
        for rnd in range(100):
            for p in self.SAMPLES:
                temp_dist = float('inf')
                temp_parent = []
                for c in self.CENTERS:
                    this_parent , dist_to_this = self.calc_scores_andparent(self.CENTERS[c],self.SAMPLES[p])
                    if dist_to_this < temp_dist:
                        temp_dist = dist_to_this
                        belongsto = c
                        temp_parent = this_parent
                        tomove = p
                    
                if tomove in self.BELONGS:
                    prev_cluster = self.BELONGS[tomove]
                    self.CLUSTERS[prev_cluster].remove(tomove)
                    ## Update the cluster we're moving this from
                    self.update_clusters_frommembers(prev_cluster)
                self.CLUSTERS[belongsto].append(tomove)
                # update the cluster we're moving this to
                self.CENTERS[belongsto] = [s for s in temp_parent]
                self.BELONGS[tomove] = belongsto
        
        
        print self.CLUSTERS
    ######################################################    
    ######################################################
    ######################################################
    def find_closest_to_centers_realkmeans(self, num_members):
        full_clusters = []
        assigned = []
        rnd = 0
        while rnd < 200:           
            flag = False
            change = 0 
            for p in self.SAMPLES:
                if p not in assigned:
                    closest = float("inf")
                    distfromcluster = 0
                    for c in self.CLUSTERS:
                        #if len(self.CLUSTERS[c]) > 0:
                        if c not in  full_clusters and len(self.CLUSTERS[c]) > 0  :
                            for m in self.CLUSTERS[c]:
                                this_parent , dist_to_this = self.score_dict[m][p]
                                distfromcluster += dist_to_this
                            distfromcluster = distfromcluster/len(self.CLUSTERS[c])
                            if distfromcluster < closest :
                                if p not in self.BELONGS:
                                    closest = distfromcluster
                                    joined = (p,c)
                                    flag = True
                                    toberemovedfrom = "XX"
                                elif self.BELONGS[p] != c and len(self.CLUSTERS[self.BELONGS[p]]) > 1:
                                    closest = distfromcluster
                                    joined = (p,c)
                                    toberemovedfrom = self.BELONGS[p]  
                                    flag = True
            #print self.CLUSTERS
            if flag:
                change +=1
                self.CLUSTERS[joined[1]].append(joined[0])
                assigned.append(joined[0])
                if toberemovedfrom in self.CLUSTERS:
                    self.CLUSTERS[toberemovedfrom].remove(joined[0])
                self.BELONGS[joined[0]] = joined[1]
                if toberemovedfrom in full_clusters:
                    full_clusters.remove(toberemovedfrom)
                
            if len(self.CLUSTERS[joined[1]]) >= num_members :
                full_clusters.append(joined[1])
            if change == 0:
                print "breaking"
                break
            if len(assigned) == len(self.SAMPLES):
                assigned = []
                full_clusters = []
                change = 0
                rnd+=1
                
        #print self.CLUSTERS
        for C in self.CLUSTERS:
            print self.CLUSTERS[C]        
        
    ######################################################    
    ######################################################
    ######################################################        
    
    def find_closest_to_centers(self, num_members):
        full_clusters = []
        assigned = []
        for init_center in self.CLUSTERS:
            assigned.append(init_center)

        while len(assigned)!= len(self.SAMPLES):
            closest = float("inf")
            for p in self.SAMPLES:
                if p not in assigned:                    
                    for c in self.CLUSTERS:
                        distfromcluster = 0
                        #print p, c, "****"
                        if c not in  full_clusters:
                            for m in self.CLUSTERS[c]:
                                this_parent , dist_to_this = self.score_dict[m][p]
                                distfromcluster += dist_to_this
                            distfromcluster = distfromcluster/len(self.CLUSTERS[c])
                            if distfromcluster < closest:
                                closest = distfromcluster
                                joined = (p,c)
                        #print p, c, "****", distfromcluster
                        #, self.CLUSTERS[c]
            #print joined[0], joined[1], closest
            self.CLUSTERS[joined[1]].append(joined[0])
            self.BELONGS[joined[0]] = joined[1]
            assigned.append(joined[0])
            if len(self.CLUSTERS[joined[1]]) == num_members :
                full_clusters.append(joined[1])
            
        #print self.CLUSTERS
        #for C in self.CLUSTERS:
        #   print self.CLUSTERS[C]
                        
    def filter_insertions_otherlevels(self):
        samplescopy = defaultdict(dict)
        for k in self.SAMPLES:
            #print "|", k
            for i in self.SAMPLES[k]:
                insertion = i[0]
                if len(insertion) > const.INS_MIN and len(insertion) < const.INS_MAX :
                    samplescopy[k][i] = 1
        self.SAMPLES = samplescopy
        

    def improving_clusters(self, rounds):
        for r in range(rounds):
            newdist = defaultdict(dict)
            current = dict()
            possibility = defaultdict(list)
            posslist = []
            for s in self.SAMPLES:
                for c in self.CLUSTERS:
                    distfromcluster= 0
                    for m in self.CLUSTERS[c]:
                        this_parent , dist_to_this = self.score_dict[m][s]
                        distfromcluster += dist_to_this
                        if m == self.BELONGS[s]:
                            current[s] = distfromcluster
                    distfromcluster = distfromcluster/len(self.CLUSTERS[c])
                    newdist[s][c] = distfromcluster
    
            
            for i in newdist:
                mindist = current[i]
                flag = False
                for j in newdist[i]:
                    if True:
                    #if not j == self.BELONGS[i]:
                        #if newdist[i][j] < mindist:
                        flag = True
                        mindist = newdist[i][j]
                        candidate = (i,j)
                        delta = current[i] - mindist
                        #if flag :
                        possibility[(self.BELONGS[candidate[0]], candidate[1])].append((candidate[0], delta))
                        posslist.append((self.BELONGS[candidate[0]], candidate[1], candidate[0], delta))
            
            swapped = []
            posslistsorted = sorted(posslist,key=lambda x: x[3], reverse = True)
            #print posslistsorted
            for indf in range(len(posslistsorted)):
                if posslistsorted[indf][2] not in swapped:
                    for gind in range(indf+1,len(posslistsorted)):
                        if posslistsorted[indf][2] not in swapped and posslistsorted[gind][2] not in swapped:
                            othersource = posslistsorted[gind][0]
                            other = posslistsorted[gind][2]
                            thissource=  posslistsorted[indf][0]
                            thisone = posslistsorted[indf][2]
                            if posslistsorted[gind][0] == posslistsorted[indf][1] and \
                            posslistsorted[gind][1] == posslistsorted[indf][0] and \
                            posslistsorted[gind][3] + posslistsorted[indf][3] :
                                #print posslistsorted[gind][3] , posslistsorted[indf][3]
                                #print thisone, other, thissource
                                self.CLUSTERS[othersource].remove(other)
                                self.CLUSTERS[thissource].remove(thisone)
                                self.CLUSTERS[thissource].append(other)
                                self.CLUSTERS[othersource].append(thisone)
                                swapped.append(thisone)
                                self.BELONGS[thisone] =  othersource
                                swapped.append(other)
                                self.BELONGS[other] = thissource
                            
            #print "*********************************************************"                
            #for C in self.CLUSTERS:
            #    print self.CLUSTERS[C]    
    ##################################################################
                            
    ## To avoid calculating a parent go with distances
    def calculate_clusterdistances(self):
        clusternames = self.CLUSTERS.keys()    
        newscore_dict = defaultdict(dict)
        sumdist = 0 
        selfsumdist = 0.0
        for c1ind in range(len(clusternames)):    
            for c2ind in range(c1ind, len(clusternames)):
                this_cluster = self.CLUSTERS[clusternames[c1ind]]
                that_cluster = self.CLUSTERS[clusternames[c2ind]]
                tempdist = 0
                for this_sample in this_cluster:
                    for that_sample in that_cluster:
                        parent, dist = self.score_dict[this_sample][that_sample]
                        tempdist+= dist
                newscore_dict[clusternames[c1ind]][clusternames[c2ind]] = (parent , tempdist)
                newscore_dict[clusternames[c2ind]][clusternames[c1ind]] = (parent , tempdist)
                if c1ind!=c2ind:
                    sumdist += tempdist
                else:
                    selfsumdist += tempdist
        #sumdist, selfsumdist,                    
        self.score_dict = newscore_dict
        return sumdist/selfsumdist 
        
    ##################################################################
    
    
    
    def improving_clusters_3members(self, rounds):
        for r in range(rounds):
            newdist = defaultdict(dict)
            current = dict()
            posslist = []
            for s in self.SAMPLES:
                for c in self.CLUSTERS:
                    newdist[s][c] =[]
                    distfromcluster = {}
                    for m in self.CLUSTERS[c]:
                        distfromcluster[m] = 0
                    
                    for m in self.CLUSTERS[c]:
                        for mk in self.CLUSTERS[c]:
                            if m != mk:
                                this_parent , dist_to_this = self.score_dict[m][s]
                                distfromcluster[mk] += dist_to_this
                    #print distfromcluster , s
                    if c == self.BELONGS[s]:
                        current[s] = distfromcluster[s]/(len(self.CLUSTERS[c])-1)
                    #print distfromcluster
                    for kk in distfromcluster:
                        #print s,c, kk
                        thisdistfromcluster = distfromcluster[kk]/(len(self.CLUSTERS[c])-1)
                        #newdist[s][c][kk] = thisdistfromcluster
                        newdist[s][c].append((kk, thisdistfromcluster))
    
            
            for i in newdist:
                mindist = current[i]
                flag = False
                ## j other cluster
                for j in newdist[i]:
                    ## e the removed one to be swapped one
                    for nd in newdist[i][j]:                 
                    #for e in newdist[i][j]:
                        #if not j == self.BELONGS[i]:
                        #if newdist[i][j] < mindist:
                        flag = True
                        e, mindist = nd #newdist[i][j]
                        candidate = (i,j,e)
                        #print current[i], mindist
                        delta = current[i] - mindist
                        #if flag :
                        posslist.append((self.BELONGS[candidate[0]], candidate[1], candidate[2], candidate[0], delta))
            
            swapped = []
            posslistsorted = sorted(posslist,key=lambda x: x[4], reverse = True)
            #print posslistsorted
            for indf in range(len(posslistsorted)):
                if posslistsorted[indf][2] not in swapped:
                    for gind in range(indf+1,len(posslistsorted)):
                        if posslistsorted[gind][3] == posslistsorted[indf][2] and\
                        posslistsorted[gind][2] == posslistsorted[indf][3]:                            
                            if posslistsorted[indf][3] not in swapped and posslistsorted[gind][3] not in swapped:
                                #print posslistsorted[gind][4] + posslistsorted[indf][4]
                                othersource = posslistsorted[gind][0]
                                other = posslistsorted[gind][3]
                                thissource=  posslistsorted[indf][0]
                                thisone = posslistsorted[indf][3]
                                if posslistsorted[gind][0] == posslistsorted[indf][1] and \
                                posslistsorted[gind][1] == posslistsorted[indf][0] and \
                                posslistsorted[gind][4] + posslistsorted[indf][4] > 1 :
                                    #print posslistsorted[gind][4] , posslistsorted[indf][4]
                                    #print thisone, other
                                    self.CLUSTERS[othersource].remove(other)
                                    self.CLUSTERS[thissource].remove(thisone)
                                    self.CLUSTERS[thissource].append(other)
                                    self.CLUSTERS[othersource].append(thisone)
                                    swapped.append(thisone)
                                    self.BELONGS[thisone] =  othersource
                                    swapped.append(other)
                                    self.BELONGS[other] = thissource
                            
            #print "*********************************************************"                
            #for C in self.CLUSTERS:
            #    print self.CLUSTERS[C]              
        
                
    def fill_samples(self):
        all_samples = ["530_1-1-1", "531_1-1-2", "532_1-1-3", "533_1-1-4", "534_1-2-1", "535_1-2-2", "536_1-2-3", "537_1-2-4", "538_1-3-1", "539_1-3-2", "540_1-3-3", "541_1-3-4", "542_2-1-1", "543_2-1-2", "544_2-1-3", "545_2-1-4", "546_2-2-1", "547_2-2-2", "548_2-2-3", "549_2-2-4", "550_2-3-1", "551_2-3-2", "552_2-3-3", "553_2-3-4",\
        "554_3-1-1", "555_3-1-2", "556_3-1-3", "557_3-1-4", "558_3-2-1", "559_3-2-2", "560_3-2-3", "561_3-2-4", "562_3-3-1", "563_3-3-2", "564_3-3-3", "565_3-3-4"]
        #"542_2-1-1", "543_2-1-2", "544_2-1-3", "545_2-1-4", "546_2-2-1", "547_2-2-2", "548_2-2-3", "549_2-2-4", "550_2-3-1", "551_2-3-2", "552_2-3-3", "553_2-3-4", "554_3-1-1", "555_3-1-2", "556_3-1-3", "557_3-1-4", "558_3-2-1", "559_3-2-2", "560_3-2-3", "561_3-2-4", "562_3-3-1", "563_3-3-2", "564_3-3-3", "565_3-3-4"]
        #const.INS_MIN =8
        #const.MAX_DIFF = 4
        #all_samples = ['531_1-1-2', '533_1-1-4', '530_1-1-1', '532_1-1-3', '535_1-2-2', '534_1-2-1', '537_1-2-4', '536_1-2-3', '539_1-3-2', '541_1-3-4', '540_1-3-3', '538_1-3-1']        
        #all_samples = ['543_2-1-2', '544_2-1-3', '545_2-1-4', '547_2-2-2', '542_2-1-1', '546_2-2-1', '552_2-3-3', '548_2-2-3', '551_2-3-2', '553_2-3-4', '550_2-3-1', '549_2-2-4']
        #all_samples = ['561_3-2-4', '565_3-3-4', '558_3-2-1', '562_3-3-1', '563_3-3-2', '564_3-3-3', '560_3-2-3', '559_3-2-2', '557_3-1-4', '554_3-1-1', '556_3-1-3', '555_3-1-2']        
        
        #filtered
        #all_samples = ["530_1-1-1", "532_1-1-3", "533_1-1-4", "534_1-2-1", "535_1-2-2", "536_1-2-3", "538_1-3-1", "539_1-3-2", "540_1-3-3", "541_1-3-4", "542_2-1-1", "544_2-1-3", "545_2-1-4", "546_2-2-1", "547_2-2-2", "548_2-2-3", "550_2-3-1", "551_2-3-2", "552_2-3-3", "553_2-3-4",\
        #"554_3-1-1", "556_3-1-3", "557_3-1-4", "558_3-2-1", "559_3-2-2", "560_3-2-3", "562_3-3-1", "563_3-3-2", "564_3-3-3", "565_3-3-4"]
        
        shuffle(all_samples)
        #print all_samples
        
        self.SAMPLES , ins_info_list = ioutil.extract_all_modinfo(all_samples)
        #print all_samples
        for s in self.SAMPLES:
            print s, len(self.SAMPLES[s])
        self.calculate_all_scores(False)
        #print "scoring is done"
        self.choose_random_centers(12)
        #print "centers are chosen"
        #print self.CLUSTERS
        self.find_closest_to_centers(8)
        #self.improving_clusters_3members(3)
        #print self.CLUSTERS
        #print "*********************************************************"
        for C in self.CLUSTERS:
            print C, self.CLUSTERS[C]
        
        
        ratio = self.calculate_clusterdistances()
        
        
        #self.CLUSTERS = defaultdict(list)
        #self.score_dict = defaultdict(dict)
        #self.SAMPLES = defaultdict(list)
        #self.BELONGS = dict()
        '''
        print "scoring is done"
        self.SAMPLES = self.score_dict.keys()
        self.CLUSTERS = defaultdict(list)
        self.choose_random_centers(3)
        print "centers are chosen"
        #print self.CLUSTERS
        self.find_closest_to_centers(3)
        
        '''
        
        '''
        self.NEWSAMPLES = defaultdict(list)      
        for f in self.CLUSTERS:
            self.NEWSAMPLES[f] = self.update_clusters_frommembers(f)
        self.SAMPLES = self.NEWSAMPLES
        shuffle(self.SAMPLES)
        const.INS_MIN = 4
        const.INS_MAX = 40
        const.MAX_DIFF = 8
        
        self.filter_insertions_otherlevels()
        self.CLUSTERS = defaultdict(list)

        self.calculate_all_scores(False)
        print "scoring is done"
        self.choose_random_centers(3)
        print "centers are chosen"
        self.find_closest_to_centers(3)
        self.improving_clusters_3members(3)
        print "*********************************************************"
        for C in self.CLUSTERS:
            print self.CLUSTERS[C]
        '''
        return const.INS_MIN, const.MAX_DIFF, ratio
        

         
    def fill_samples_realkmeans(self):
        all_samples = ["530_1-1-1", "531_1-1-2", "532_1-1-3", "533_1-1-4", "534_1-2-1", "535_1-2-2", "536_1-2-3", "537_1-2-4", "538_1-3-1", "539_1-3-2", "540_1-3-3", "541_1-3-4", "542_2-1-1", "543_2-1-2", "544_2-1-3", "545_2-1-4", "546_2-2-1", "547_2-2-2", "548_2-2-3", "549_2-2-4", "550_2-3-1", "551_2-3-2", "552_2-3-3", "553_2-3-4",\
         "554_3-1-1", "555_3-1-2", "556_3-1-3", "557_3-1-4", "558_3-2-1", "559_3-2-2", "560_3-2-3", "561_3-2-4", "562_3-3-1", "563_3-3-2", "564_3-3-3", "565_3-3-4"]
        # "542_2-1-1", "543_2-1-2", "544_2-1-3", "545_2-1-4", "546_2-2-1", "547_2-2-2", "548_2-2-3", "549_2-2-4", "550_2-3-1", "551_2-3-2", "552_2-3-3", "553_2-3-4", "554_3-1-1", "555_3-1-2", "556_3-1-3", "557_3-1-4", "558_3-2-1", "559_3-2-2", "560_3-2-3", "561_3-2-4", "562_3-3-1", "563_3-3-2", "564_3-3-3", "565_3-3-4"]
        shuffle(all_samples)
        self.SAMPLES , ins_info_list = ioutil.extract_all_modinfo(all_samples)
        print all_samples
        self.calculate_all_scores(False)
        print "scoring is done"
        self.choose_random_centers_forrealkmeans(9)
        print "centers are chosen"
        print self.find_closest_to_centers_realkmeans(4)
'''   
maxr = 0
for f in range(4,12):
    for k in range(4,12):
        print f,k              
        const.INS_MIN = f
        const.MAX_DIFF = k 
        lk = LIKEKMEANS()
        im, md, r = lk.fill_samples()
        print f,k, r
        if r > maxr:
            maxr = r
            imf = im
            mdf = md
'''
lk = LIKEKMEANS()
imf = 7
mdf = 0
const.INS_MIN = imf
const.MAX_DIFF = mdf
#print maxr, imf, mdf
#im, md, r = 
lk.fill_samples()
#lk.fill_samples_realkmeans()
#
                


