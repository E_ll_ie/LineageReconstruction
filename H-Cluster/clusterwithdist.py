import sys
import const
from collections import defaultdict
import scipy.cluster.hierarchy as hcl
from scipy.spatial.distance import squareform
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import ioutil


###############################################################################
## This is how I score now which might not be the best idea
## This is just the longest prefix
## Averaged by length of sequences
###############################################################################
def calc_common(seq1, seq2):
    common = 0.0
    if len(seq1) > len(seq2):
        longest = seq1
        shortest = seq2
    else:
        longest = seq2
        shortest = seq1
    for indd in range((min(len(seq1), len(seq2)))):
        if seq1[indd] == seq2[indd]:
            common +=1
        else:
            break
    if max(len(seq1), len(seq2)) - common <= const.MAX_DIFF and common > 0:
        #print common
        #return seq1[:int(common)],  1/common #((common/len(seq1)) + (common/len(seq2)))/2 #1/common
        #return seq1[:int(common)], (((len(seq1)-common)/len(seq1)) + ((len(seq2)-common)/len(seq2)))/2
        #return [seq1[:int(common)]] , (((len(seq1)-common)/len(seq1)) + ((len(seq2)-common)/len(seq2)))/2
        return seq1[:int(common)], 0# (((len(seq1)-common)) + ((len(seq2)-common)))/2
    else:
        #return seq1[:int(common)], 10
        return seq1[:int(common)] ,  const.PENALTY  #10 
        #return seq1[:int(common)], (((len(seq1)-common)) + ((len(seq2)-common)))*const.PENALTY/2

###############################################################################
## This compares two list of insertions
## It might be the two samples
## It might be one sample and a masterlist
###############################################################################
def calc_sumdist(this_sampled, list_of_insertions):
    this_row  = dict()
    parent_set = dict()
    list_of_insertions_modified = {}
    blacks = set([])
    ## I actually first pair the sequences present in both
    ## If there's none, I'll keep the insertions in another list
    for insertion in list_of_insertions:
        if insertion in this_sampled:
            if const.APPLYWEIGHT:
                weight = min(this_sampled[insertion], list_of_insertions[insertion])/ max(this_sampled[insertion], list_of_insertions[insertion])
            else:
                weight = 1.0
            this_row[insertion] = 0.0
            #this_row[insertion] = 1*weight
            blacks.add(insertion)
            parent_set[insertion] = 1
            #parent_set.append(insertion)
        else:
            #print insertion
            list_of_insertions_modified[(insertion[0], insertion[1])] = list_of_insertions[insertion]

    for insertion in list_of_insertions_modified:
        ## Insertion and Position
        
        inss, insp = insertion[0], insertion[1]
        this_score = const.PENALTY 
        possible_parent =""
        sim_seq = ("", 0.0)
        this_row[insertion] = 10.0
        #parent_set[""]=1
        
        ## Duplicated condition checking ,  I mean this if shouldn't happen
        ## I already remove them
        #if (insertion) in this_sampled:
        #    this_score = 0
        if True:
            for k in this_sampled:
                if len(k) == 2:
                    if const.APPLYWEIGHT:
                        weight = min(this_sampled[k], list_of_insertions_modified[insertion])/ max(this_sampled[k], list_of_insertions_modified[insertion])
                    else:
                        weight = 1.0
                    ## Looking for the maximum similarities
                    if k[1] == insp and k not in blacks:
                        temp_parent, temp_score =  calc_common(inss, k[0])
                        temp_score*= weight
                        if len(temp_parent) > len(possible_parent):                        
                        #if temp_score > this_score:
                            this_score = temp_score
                            sim_seq = k
                            possible_parent = temp_parent
                ## If we could find one here 
                this_row[insertion] = this_score
                if this_score == 0 :
                     blacks.add(sim_seq)
                '''
                    if possible_parent == "":
                        possible_parent = ("", -1)
                        parent_set[("", -2)] = 1
                    if len(possible_parent) >= 1:
                        parent_set[(possible_parent[0], k[1])] = 1
                    if  len(possible_parent) > 1:
                        parent_set[(possible_parent[1], k[1])] = 1
                    #parent_set.append(possible_parent)
                    blacks.add(sim_seq)
                '''
    #print float(this_row.values().count(1))/len(list_of_insertions)
    
    return this_row, parent_set

###############################################################################
## For samples calculate a general sum score (??)
###############################################################################
def calc_dist(this_sampled, that_sampled):
    
    if len(this_sampled) > len(that_sampled):
        outputsum, outputparents = calc_sumdist(this_sampled, that_sampled)
        #FIXME:
        ## Not sure if normalization this way is necessary 
        this_score = sum(outputsum.values())*1.0/(len(that_sampled))
    else:
        outputsum, outputparents = calc_sumdist(that_sampled, this_sampled)
        ## Not sure if normalization this way is necessary 
        this_score = sum(outputsum.values())*1.0/(len(this_sampled))


    return this_score

###############################################################################
## Calculating table of pairwise distances
## use that to cluster
## Not the best way though
###############################################################################
def calculate_score_for_pairs(info_dict):
    A = []
    B = []
    D = []
    names = info_dict.keys()
    for indd in range(len(names)):
        for otherindd in range(indd, len(names)):
            print names[indd], names[otherindd]
            print indd, otherindd
            A.append(names[indd])
            B.append(names[otherindd])            
            #FIXME:
            # What about this 1 - something here:
            #D.append(calc_dist(info_dict[names[indd]], info_dict[names[otherindd]]))
            D.append(1.0 - calc_dist(info_dict[names[indd]], info_dict[names[otherindd]]))
    data = pd.DataFrame({ "A" : A, "B" : B, "D" : D})
    data_piv = data.pivot("A", "B", "D").fillna(0)

    piv_arr = data_piv.as_matrix()
    dist_mat = piv_arr + np.transpose(piv_arr)
    print dist_mat
    linkarray = hcl.linkage(squareform(dist_mat))
    print names
    print linkarray
    plt.figure()
    hcl.dendrogram(linkarray, labels = names)
    plt.show()

###############################################################################
## same as above, only with 1 and 0s
###############################################################################
def calculate_by01(samples,info_dict, info_list):
    all_mat = [[] for i in range(len(samples))]
    for ind in range(len(samples)):
        for inst in info_list:
            if inst in info_dict[samples[ind]]:
                all_mat[ind].append(1)
            else:
                all_mat[ind].append(0)
    names = samples
    A = []
    B = []
    D = []
    for indd in range(len(names)):
        for otherindd in range(indd, len(names)):
            A.append(names[indd])
            B.append(names[otherindd])
            D.append(sum([0 if all_mat[indd][i] == all_mat[otherindd][i] else 1 for i in range(len(all_mat[indd]))]))
    data = pd.DataFrame({ "A" : A, "B" : B, "D" : D})
    data_piv = data.pivot("A", "B", "D").fillna(0)

    piv_arr = data_piv.as_matrix()
    dist_mat = piv_arr + np.transpose(piv_arr)
    linkarray = hcl.linkage(squareform(dist_mat))
    print names
    print linkarray
    plt.figure()
    hcl.dendrogram(linkarray, labels = names)
    plt.show()

###############################################################################

###############################################################################
def main():
    all_samples = ["530_1-1-1", "531_1-1-2", "532_1-1-3", "533_1-1-4", "534_1-2-1", "535_1-2-2", "536_1-2-3", "537_1-2-4", "538_1-3-1", "539_1-3-2", "540_1-3-3", "541_1-3-4", "542_2-1-1", "543_2-1-2", "544_2-1-3", "545_2-1-4", "546_2-2-1", "547_2-2-2", "548_2-2-3", "549_2-2-4", "550_2-3-1", "551_2-3-2", "552_2-3-3", "553_2-3-4", "554_3-1-1", "555_3-1-2", "556_3-1-3", "557_3-1-4", "558_3-2-1", "559_3-2-2", "560_3-2-3", "561_3-2-4", "562_3-3-1", "563_3-3-2", "564_3-3-3", "565_3-3-4"]

    ins_info_dict , ins_info_list = ioutil.extract_all_modinfo(all_samples)
    #calculate_score_for_pairs(ins_info_dict)
    calculate_by01(all_samples, ins_info_dict , ins_info_list)
            
    


#main()
