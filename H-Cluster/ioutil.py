import sys
import const
from collections import defaultdict
###############################################################################
## Right now Im not using the other part of the sequence
## Only the Insertions
## This file is the init file: .withinfo file
## We have sequence / mapped/ position / insertion
###############################################################################
def extract_each_file_info(this_file_dir, all_ins_set):
    this_file = open(this_file_dir,'r').read().splitlines()
    this_dict = {}
    for line in this_file:
        fds = line.split()
        ins_pos = int(fds[2])
        insertion = fds[3]
        if len(insertion) > const.INS_MIN and len(insertion) < const.INS_MAX:
            if (insertion, ins_pos) in this_dict:
                this_dict[(insertion, ins_pos)]+= 1
            else:
                this_dict[(insertion, ins_pos)] = 1
                all_ins_set.add((insertion, ins_pos))
    for f in this_dict:
        this_dict[f] = this_dict[f]*1.0/len(this_file)
    return this_dict, all_ins_set

###############################################################################
## But I also have the modified version now, so we can just use that
## 40      CCGAAAAGGAGCACGTACCT    20      1       0.0587544065805
###############################################################################
def extract_each_mod_file(this_file_dir, all_ins_set):
    this_file = open(this_file_dir,'r').read().splitlines()
    this_dict = {}
    
    lls = set()
    ccs = set()
    #######################################
    ## To get the less frequents 
    ## Or to get the longest
    for line in this_file:
        fds = line.split()
        lls.add(len(fds[1]))
        ccs.add(int(fds[-2]))
    lls = list(lls)
    ccs = list(ccs)
    lls.sort(reverse = True)
    ccs.sort()
    lls = lls[:int(const.TOPP*len(lls))]
    ccs = ccs[:int(const.TOPP*len(ccs))]
    ## I know it's not clean
    ######################################
    
    for line in this_file:
        fds = line.split()
        ins_pos = int(fds[0])
        insertion = fds[1]
        ## I dont even know if I need the root but lets keep it for now
        if insertion != "ROOT":
            flag = True
            if const.APPLYCOUNTLIMIT:
                if not(int(fds[-2]) > const.COUNTLIMIT[0] and int(fds[-2]) < const.COUNTLIMIT[1]):
                    flag = flag and False
            if const.APPLYPERCLIMIT:
                if not(float(fds[-1]) > const.PERCLIMIT[0] and float(fds[-1]) < const.PERCLIMIT[1]):
                    flag = flag and False   
            if const.APPLYTOPLENGTH:
                if not(len(insertion)) in lls:
                    flag = flag and False
            if const.APPLYTOPCOUNT:
                if not(float(fds[-1])) in ccs:
                    flag = flag and False
            if not (len(insertion) > const.INS_MIN and len(insertion) < const.INS_MAX):
                flag = flag and False
        else:
            flag = True
        if flag:
            this_dict[(insertion, ins_pos)] = float(fds[-1])
            all_ins_set.add((insertion, ins_pos))
            #this_dict[(insertion, 3)] = float(fds[-1])
            #all_ins_set.add((insertion, 3))
    #print len(this_dict.keys())
    #print this_dict.keys()
    return this_dict, all_ins_set
    
###############################################################################    
## Loop over all files and make a dict of insertion for each file
## Plus a set of insertions
###############################################################################
def extract_all_info(list_of_files):
    info_dict = defaultdict(dict)
    all_insertions = set([])
    for this_f in list_of_files:
        info_dict[this_f], all_insertions = extract_each_file_info(this_f+".newinfo.txt", all_insertions)
    
    return info_dict, all_insertions
    
    
###############################################################################
## Loop over all files and make a dict of insertion for each file
## Plus a set of insertions
###############################################################################
def extract_all_modinfo(list_of_files):
    info_dict = defaultdict(dict)
    all_insertions = set([])
    for this_f in list_of_files:
        #info_dict[this_f.split("_")[1]], all_insertions = extract_each_mod_file(r'C:\Users\Elmira\Desktop\Work\Fall2017\LiuLab\November\umi2\5' +this_f[1:]+".newinfo.withlengths.sorted.txt", all_insertions)
        #info_dict[this_f.split("_")[1]], all_insertions = extract_each_mod_file(r'C:\Users\Elmira\Desktop\Work\Fall2017\LiuLab\December\Insertions2\5' +this_f[1:]+".newinfo.withlengths.sorted.txt", all_insertions)
        #C:\Users\Elmira\Desktop\Work\Winter2018\Theresa\lt
        #info_dict[this_f.split("_")[1]], all_insertions = extract_each_mod_file(r"C:\Users\Elmira\Desktop\Work\Winter2018\Theresa\insertions\2\5" +this_f[1:]+".newinfo.withlengths.sorted.txt", all_insertions)
        info_dict[this_f.split("_")[1]], all_insertions = extract_each_mod_file(r'C:\Users\Elmira\Desktop\Work\Fall2017\LiuLab\Holiday\Insertions\5' +this_f[1:]+".newinfo.withlengths.sorted.txt", all_insertions)
    return info_dict, all_insertions
