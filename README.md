 To be completed.
 
 Besides Numpy, the following libraries/tools might be required for running some of the scripts:
 
 
 1) regex : https://pypi.python.org/pypi/regex
 
 2) motility : https://github.com/ctb/motility
 
 3) PEAR (already uploaded here): https://sco.h-its.org/exelixis/web/software/pear/
 
 
 "mapp" is the executable version of the same implementation of optimal sequence alignment used in (Perli et al, 2016) and the c++ code had made available here by the authors of that paper :
 http://www.rle.mit.edu/sbg/resources/stgRNA/
 
 
 I) Perli, S. D., Cui, C. H. & Lu, T. K. Continuous genetic recording with self-targeting CRISPR-Cas in human cells. Science 353, aag0511 (2016).
 
 II) Zhang, J., Kobert, K., Flouri, T. & Stamatakis, A. PEAR: a fast and accurate Illumina Paired-End reAd mergeR. Bioinformatics 30, 614–620 (2014).